﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarsalesChallenge.Models
{
    public class CarDto
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public byte Doors { get; set; }
        public string Color { get; set; }
    }
}