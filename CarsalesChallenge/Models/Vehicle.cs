﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace CarsalesChallenge.Models
{
    public abstract class Vehicle
    {
        [Key]
        public int Id { get; set; }
        [DefaultValue("car")]
        [ScaffoldColumn(false)]
        public string VehicleType { get; set; }



        [Required]
        public decimal Price { get; set; }

        public  CarModel CarModel { get; set; }

        public int CarModelId { get; set; }

    }
}