﻿using System.ComponentModel.DataAnnotations;

namespace CarsalesChallenge.Models
{
    public class Car : Vehicle
    {
        //[RegularExpression("[1-8]", ErrorMessage = "Invalid doors number")]
        //public byte Doors { get; set; }
        //public string BodyType { get; set; }

        public string Color { get; set; }
    }
}