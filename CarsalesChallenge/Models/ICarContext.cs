﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CarsalesChallenge.Models
{
    public interface ICarContext : IDisposable
    {
        DbSet<Car> Cars { get;  }

        DbSet<Make> Makes { get;  }

        DbSet<CarModel> CarModel { get;  }

        Task<int> SaveChangesAsync();
        void MarkAsModified(Car item);
    }
}
