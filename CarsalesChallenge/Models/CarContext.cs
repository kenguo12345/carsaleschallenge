﻿
using System.Data.Entity;


namespace CarsalesChallenge.Models
{
    public class CarContext : DbContext, ICarContext
    {

    
        public CarContext() : base("name=CarContext")
        {
        }

        public virtual DbSet<Car> Cars { get; set; }

        public virtual DbSet<Make> Makes { get; set; }

        public virtual DbSet<CarModel> CarModel { get; set; }




        public void MarkAsModified(Car car)
        {
            Entry(car).State = EntityState.Modified;
        }
    }
}
