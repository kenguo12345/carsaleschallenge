﻿
using System.ComponentModel.DataAnnotations;


namespace CarsalesChallenge.Models
{
    public class CarModel
    {
        [Key]
        public int CarModelId { get; set; }
        [Required]
        public string ModelName { get; set; }
        [RegularExpression("[1-8]", ErrorMessage = "Invalid doors number")]
        public byte Doors { get; set; }
        [RegularExpression("(?i)SUV|(?i)hatch|(?i)Wagon|(?i)UTE|(?i)Coupe|(?i)Sedan", ErrorMessage = "Invalid Vehicle Type")]
        public string BodyType { get; set; }

        public virtual Make Make { get; set; }
        [Required]
        public int MakeId { get; set; }

    }
}