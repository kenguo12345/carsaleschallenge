﻿
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CarsalesChallenge.Models;
using System.Net;

namespace CarsalesChallenge.Controllers
{
    public class CarsController : ApiController
    {

        private ICarContext db = new CarContext();

        public CarsController() { }

        public CarsController(ICarContext context)
        {
            db = context;
        }


        // GET: api/Cars
        public IQueryable<CarDto> GetCars()
        {
            var cars = from c in db.Cars
                       select new CarDto()
                       {
                           Id = c.Id,
                           Make = c.CarModel.Make.MakeName,
                           Model = c.CarModel.ModelName,
                           Doors = c.CarModel.Doors,
                           Price = c.Price,
                           Color = c.Color

                       };

            return cars;
        }

        // GET: api/Cars/5
        [ResponseType(typeof(CarDto))]
        public async Task<IHttpActionResult> GetCar(int id)
        {
            // Car car = await db.Cars.FindAsync(id);
            var car = await db.Cars.Include(c => c.CarModel).Select(c =>
                  new CarDto()
                  {
                      Id = c.Id,
                      Make = c.CarModel.Make.MakeName,
                      Model = c.CarModel.ModelName,
                      Doors = c.CarModel.Doors,
                      Price = c.Price,
                      Color = c.Color
                  }).SingleOrDefaultAsync(c => c.Id == id);
            if (car == null)
            {
                return NotFound();
            }

            return Ok(car);
        }

        // PUT: api/Cars/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCar(int id, Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != car.Id)
            {
                return BadRequest("Query Id does not match Id from body");
            }


            //  db.Entry(car).State = EntityState.Modified;
            db.MarkAsModified(car);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarExists(id))
                {
                    return NotFound();
                }

                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cars
        [ResponseType(typeof(Car))]
        public async Task<IHttpActionResult> PostCar(Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!ModelExists(car.CarModelId))
            {
                return BadRequest("CarModelId not exist or missing");
            }

            db.Cars.Add(car);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = car.Id }, car);
        }

        // DELETE: api/Cars/5
        [ResponseType(typeof(CarDto))]
        public async Task<IHttpActionResult> DeleteCar(int id)
        {
            Car car = await db.Cars.FindAsync(id);
            if (car == null)
            {
                return NotFound();
            }

            db.Cars.Remove(car);
            await db.SaveChangesAsync();

            return Ok(car);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CarExists(int id)
        {
            return db.Cars.Count(e => e.Id == id) > 0;
        }
        private bool ModelExists(int id)
        {
            return db.CarModel.Count(e => e.CarModelId == id) > 0;
        }
    }
}