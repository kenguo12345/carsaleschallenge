namespace CarsalesChallenge.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CarsalesChallenge.Models.CarContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CarsalesChallenge.Models.CarContext context)
        {
            context.Makes.AddOrUpdate(
            new Make { MakeId = 1, MakeName = "Audi"  },
            new Make { MakeId = 2, MakeName = "BMW" },
            new Make { MakeId = 3, MakeName = "Toyota" }

                );
            context.CarModel.AddOrUpdate(
            new CarModel { CarModelId = 1, ModelName = "X3", Doors = 2, BodyType = "Hatch",MakeId =2 },
            new CarModel { CarModelId = 2, ModelName = "Land Crusier", Doors = 4, BodyType = "SUV", MakeId =3 },
            new CarModel { CarModelId = 3, ModelName = "A4", Doors = 2, BodyType = "Sedan", MakeId =1 },
            new CarModel { CarModelId = 4, ModelName = "A4", Doors = 4, BodyType = "Sedan", MakeId = 1 }

                );


            context.Cars.AddOrUpdate(x => x.Id,
            new Car { Id = 123, VehicleType = "car", CarModelId = 1, Price = 23444.12m , Color="Red"  },
            new Car { Id = 223, VehicleType = "car", CarModelId = 3, Price = 65444.12m , Color="Green" },
            new Car { Id = 323, VehicleType = "car", CarModelId = 2, Price = 24644.12m , Color=null}
                );
        }
    }
}
