namespace CarsalesChallenge.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarModels",
                c => new
                    {
                        CarModelId = c.Int(nullable: false, identity: true),
                        ModelName = c.String(nullable: false),
                        Doors = c.Byte(nullable: false),
                        BodyType = c.String(),
                        MakeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CarModelId)
                .ForeignKey("dbo.Makes", t => t.MakeId, cascadeDelete: true)
                .Index(t => t.MakeId);
            
            CreateTable(
                "dbo.Makes",
                c => new
                    {
                        MakeId = c.Int(nullable: false, identity: true),
                        MakeName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.MakeId);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Color = c.String(),
                        VehicleType = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CarModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarModels", t => t.CarModelId, cascadeDelete: true)
                .Index(t => t.CarModelId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "CarModelId", "dbo.CarModels");
            DropForeignKey("dbo.CarModels", "MakeId", "dbo.Makes");
            DropIndex("dbo.Cars", new[] { "CarModelId" });
            DropIndex("dbo.CarModels", new[] { "MakeId" });
            DropTable("dbo.Cars");
            DropTable("dbo.Makes");
            DropTable("dbo.CarModels");
        }
    }
}
