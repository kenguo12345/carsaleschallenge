﻿
using System.Linq;
using CarsalesChallenge.Models;

namespace CarsalesChallenge.Tests
{
    class TestCarDbSet : TestDbSet<Car>
    {
        public override async System.Threading.Tasks.Task<Car> FindAsync(params object[] keyValues)
        {
            await System.Threading.Tasks.Task.Delay(20);
            return this.SingleOrDefault(car => car.Id == (int)keyValues.Single());
        }
    }
}
