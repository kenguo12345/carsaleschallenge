﻿using System;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarsalesChallenge.Models;
using CarsalesChallenge.Controllers;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Net;

namespace CarsalesChallenge.Tests
{
    [TestClass]
    public class TestCarsController
    {
        [TestMethod]
        public void GetCars_ShouldReturnAllCars()
        {

            var Makedata = new List<Make>
            {
                new Make { MakeId = 1, MakeName = "BMW" },
                new Make { MakeId = 2, MakeName = "Audi" }

            }.AsQueryable();
            var Modeldata = new List<CarModel>
            {
               new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1 },
                new CarModel { CarModelId = 2, ModelName = "A5", Doors = 2, BodyType = "Sedan", MakeId = 2 },
                 new CarModel { CarModelId = 3, ModelName = "A4", Doors = 4, BodyType = "Sedan", MakeId = 2 }

            }.AsQueryable();
            var data = new List<Car>
            {
                new Car { Id = 1, Price = 234544.12m, Color = "red",CarModelId=1,CarModel = new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1, Make =  new Make { MakeId = 1, MakeName = "BMW" }}},
                new Car { Id = 1, Price = 234544.12m, Color = "red",CarModelId=2,CarModel = new CarModel { CarModelId =2, ModelName = "A4", Doors = 4, BodyType = "SUV", MakeId = 2, Make =  new Make { MakeId = 2, MakeName = "Audi" }}},

            }.AsQueryable();




            var mockSet = GetMockDbSet(data);


            var mockContext = new Mock<CarContext>();
            mockContext.Setup(c => c.Cars).Returns(mockSet.Object);



            var controller = new CarsController(mockContext.Object);
            var result =  controller.GetCars();

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());


        }

 
        [TestMethod]
        public async Task PostCar_ShouldReturnSameCar()
        {
            var context = new TestCarContext();
            context.Makes.Add(new Make { MakeId = 1, MakeName = "BMW" });

            context.CarModel.Add(new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1 });

            context.Cars.Add(new Car { Id=1,CarModelId = 1, Price=123m });




            var controller = new CarsController(context);

            var item = GetDemoCarWithDependency();

            var result =
                await controller.PostCar(item) as CreatedAtRouteNegotiatedContentResult<Car>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "DefaultApi");
            Assert.AreEqual(result.RouteValues["id"], result.Content.Id);
            Assert.AreEqual(result.Content.Id, item.Id);
        }
        [TestMethod]
        public async Task PutCar_ShouldReturnOK()
        {
            var context = new TestCarContext();
            context.Makes.Add(new Make { MakeId = 1, MakeName = "BMW" });
            context.CarModel.Add(new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1 });
            context.Cars.Add(new Car { Id = 1, CarModelId = 1, Price = 158923m });
            var item = GetDemoCar();

            var controller = new CarsController(context);
            var result = await controller.PutCar(item.Id, item) as StatusCodeResult;

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }
        [TestMethod]
        public async Task PutCar_ShouldFail_WhenDifferentID()
        {
            var context = new TestCarContext();
            context.Makes.Add(new Make { MakeId = 1, MakeName = "BMW" });
            context.CarModel.Add(new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1 });
            context.Cars.Add(new Car { Id = 1, CarModelId = 1, Price = 158923m });
            var item = GetDemoCar();

            var controller = new CarsController(context);
            var result = await controller.PutCar(999, item) ;

            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public async Task DeleteProduct_ShouldReturnOK()
        {
            var context = new TestCarContext();
            var item = GetDemoCar();
            context.Cars.Add(item);

            var controller = new CarsController(context);
            var result = await controller.DeleteCar(1) as OkNegotiatedContentResult<Car>;

            Assert.IsNotNull(result);
            Assert.AreEqual(item.Id, result.Content.Id);
        }


        Car GetDemoCar()
        {
            return new Car() { Id = 1, Price = 44.12m, Color = "white", CarModelId = 1 };

        }
        Car GetDemoCarWithDependency()
        {
            return new Car { Id = 1, Price = 234544.12m, Color = "red", CarModelId = 1, CarModel = new CarModel { CarModelId = 1, ModelName = "X3", Doors = 4, BodyType = "SUV", MakeId = 1, Make = new Make { MakeId = 1, MakeName = "BMW" } } };

        }

        private Mock<DbSet<T>> GetMockDbSet<T>(IQueryable<T> entities) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(entities.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(entities.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(entities.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(entities.GetEnumerator());
            return mockSet;
        }


    }

}
