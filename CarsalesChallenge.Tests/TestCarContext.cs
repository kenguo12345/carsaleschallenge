﻿using CarsalesChallenge.Models;

using System.Data.Entity;


namespace CarsalesChallenge.Tests
{
    class TestCarContext: ICarContext
    {

        public TestCarContext()
        {
            this.Cars = new TestCarDbSet();
            this.Makes = new TestDbSet<Make>();
            this.CarModel = new TestDbSet<CarModel>();

        }

        public DbSet<Car> Cars { get; set; }

        public DbSet<Make> Makes { get; set; }

        public DbSet<CarModel> CarModel { get; set; }


        public void MarkAsModified(Car car)
        {

        }

        public void Dispose() { }
        public async System.Threading.Tasks.Task<int> SaveChangesAsync()
        {
            await System.Threading.Tasks.Task.Delay(20);
            return 0;
        }

    }
}
