﻿using System;
using System.IO;



namespace ConsoleApiManager
{
    class Program
    {
        const string DIR = @"C:\Users\Yeqin\Desktop\carsalesLog";
        const string FILE = @"\carlog.txt";
        const string PATH = DIR + FILE;
        const string URL = @"http://localhost:60731/api/cars";

        static void Main(string[] args)
        {
            try
            {
                if (Directory.Exists(DIR))
                {
                    while (true)
                    {
                        string data = ConsumeEventSync.GetAllEventData(URL);
                        FileIO.LogToFile(PATH, data);



                        System.Threading.Thread.Sleep(60 * 60 * 1000);

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

        }
    }
}
