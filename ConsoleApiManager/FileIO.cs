﻿using System;

using System.IO;


namespace ConsoleApiManager
{
    class FileIO
    {
        static public void LogToFile(string path, string data)
        {

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("---------{0}---------", DateTime.Now.ToString());
                    sw.WriteLine(data);

                }
                return;
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine("---------{0}---------", DateTime.Now.ToString());
                sw.WriteLine(data);
            }
        }
    }
}
