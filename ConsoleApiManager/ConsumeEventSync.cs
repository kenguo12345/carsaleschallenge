﻿using System;

using System.Net;


namespace ConsoleApiManager
{
    class ConsumeEventSync
    {
        static public string GetAllEventData(string url)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type:application/json");
                client.Headers.Add("Accept:application/json");
                string result = client.DownloadString(url);
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }
    }
}
