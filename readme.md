# Carsales Code Challenge

 First of all, I have to say this 'simple' project has
 a lot of tricky parts. I initially thought this would be an easy test but ended up finishing it with a lot of Googling. 

 In the beginning, I was planning to use a file in json format as the database. But then I realised this would cause a lot of trouble doing read/write and validattion, and most importantly, it is difficult to run unit test because mocking the Read/Write behavbiours and other unpreditable errors would not allow me to finish in two days (No wonder that unit test is encourged instead of complusory). After having a brainstorming, I chose Entity Framework as my new database. 

 Api is built by MVC framework. I used code first approach to initialise the database. Migration and seeding configuration has been done so you only need to type one command to ceate the database. DTO is used to avoid overposting. 

 Moving to Unit testing, both mocking framework test and in-memory test have been implenmented. And here is the story of my day: I was only thinking to use in-memory test but the Async functions stop me from doing this. I have encoutered an error, saying something about IQuerable, so I had to not only mock the context and DbSet, but generate some fake data to trick the contorller.

 Console application took me least time I have to say. I would have added logging if I had more time.  

 Overally, I enjoyed this code challenge. Please give me feedback so I can improve and criticise myself  :)



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Please install all the dependencies over NuGet Manager

After install all the packages, exclude ApplicationInsights.config from the Carsales project as it might cause build error(complain about this file not exist).

After that, open Package Manager Console, configure and seed the database data. The command is:

```
update-database
```

### Console Application

Please udpate the DIR and FILE in Program file to make sure the path is valid. Otherwise Dirtory not found error will throw.





```
const string DIR = @"C:\[your\valid\dirtory]";
const string FILE = @"\[LogName].txt";
```
Before start the console, right click Solution and click properties, make sure multiple startup proejcts is selected and CarsalesChallenge should start first. 







## Finally

Please contact me if you got any questions.




## Authors

* **Ken Guo** - *Initial work* - [Linkedin](https://www.linkedin.com/in/ken-guo-122058126/)




